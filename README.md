# mockake

[![Build Status](https://travis-ci.com/leominov/mockake.svg?branch=master)](https://travis-ci.com/leominov/mockake)
[![codecov](https://codecov.io/gh/leominov/mockake/branch/master/graph/badge.svg)](https://codecov.io/gh/leominov/mockake)

Mock server to collaborate with [shunit2](https://github.com/kward/shunit2).

## Usage

* `mockake start`
* `mockake status`
* `mockake --config ./test_case1.yaml set`
* `mockake flush`
* `mockake --config ./test_case2.yaml set`
* `mockake stop`

or

* `mockake --config ./test_case1.yaml start`
* `mockake stop`

After `mockake start` you also may interact with a server by API:

* `curl http://127.0.0.1:8088/-/status` – `mockake status`
* `curl -X POST http://127.0.0.1:8088/-/set --data-binary @example_test_ok.yaml` – `mockake set`
* `curl -X POST http://127.0.0.1:8088/-/flush` – `mockake flush`
* `curl --head http://127.0.0.1:8088/-/stop` – `mockake stop`

Port also may be rewritten:

```
Usage of mockake:
  -bind string
      Default address to serve incoming requests (default ":8088")
  -config string
      Configuration file
```

## How to start?

It's easy as eat a :banana:, just look an example:

* `example.sh` – code to test
* `example_test_(.*).yaml` – sets used in mock server
* `example_test.sh` – test with asserts

Before you execute `./example_test.sh` make shure that all dependencies installed:

* Google Go
* curl
* jq

## httptest

An additional tool to test HTTP resources.

### Usage

* `httptest -c .httptest.yaml`

### Configuration

Example:

```text
@options resolve=work.company.com:443/10.10.0.10:443
@test url=https://work.company.com/api code=401
@test url=https://work.company.com/api/status body.regex=ok headers.x-powered-by.prefix=PHP code.exact=200
@test url=https://work.company.com/ code=200 tls=1.1
@test url=https://work.company.com/ code=200 tls=1.2
@test url=https://work.company.com/ code=200 tls=1.3
@test url=https://work.company.com/ code=200 tls=1.4
```

or

```yaml
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: work-company-route
spec:
  # @options resolve=work.company.com:443/10.10.0.10:443
  hosts:
  - work.company.com
  http:
  - match:
    # @test url=https://work.company.com/api code=401
    # @test url=https://work.company.com/api/status body.regex=ok headers.x-powered-by.prefix=PHP code.exact=200
    - uri:
        prefix: /api
    route:
    - destination:
        host: api
  - route:
    # @test url=https://work.company.com/ code=200 tls=1.1
    # @test url=https://work.company.com/ method=POST code=200 tls=1.2
    # @test url=https://work.company.com/ method=DELETE code=200 tls=1.3
    # @test url=https://work.company.com/ method=OPTIONS code=200 tls=1.4
    - destination:
        host: ui
```

## Links

- https://hub.docker.com/repository/docker/leominov/mockake/
