FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/mockake
COPY . .
RUN CGO_ENABLED=0 go build ./cmd/mockake
RUN CGO_ENABLED=0 go build ./cmd/httptest

FROM alpine:3.11
COPY --from=builder /go/src/github.com/leominov/mockake/mockake /usr/local/bin/mockake
COPY --from=builder /go/src/github.com/leominov/mockake/httptest /usr/local/bin/httptest
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

ENTRYPOINT ["mockake"]
