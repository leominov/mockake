module github.com/leominov/mockake

go 1.14

require (
	github.com/gorilla/handlers v1.5.1
	github.com/mitchellh/mapstructure v1.3.3
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v2 v2.3.0
)
