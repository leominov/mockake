package main

import (
	"bufio"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/leominov/mockake/pkg/textutils"

	"github.com/sirupsen/logrus"
)

var (
	dialer = &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
	}

	transport = &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
)

type Testsuite struct {
	log   *logrus.Entry
	o     *Options
	cases []*Testcase
}

func NewTestsuiteFromFile(log *logrus.Entry, filename string) (*Testsuite, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	t := &Testsuite{
		log: log,
		o:   NewOptions(),
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		t.o.EnrichFromLine(line)
		tc, _ := NewTestcaseFromLine(line)
		if tc != nil {
			t.cases = append(t.cases, tc)
		}
	}

	if len(t.cases) == 0 {
		return nil, errors.New("tests not found")
	}

	return t, nil
}

func (t *Testsuite) NewHttpClientForTestcase(tc *Testcase) *http.Client {
	tr := transport.Clone()

	tr.DialContext = t.DialContext
	tr.TLSClientConfig = nil

	if tc.TLS > 0 {
		tr.TLSClientConfig = &tls.Config{
			CipherSuites: []uint16{
				tc.TLS,
			},
		}
	}

	if t.o.Insecure {
		tr.TLSClientConfig = &tls.Config{
			InsecureSkipVerify: true,
		}
	}

	client := &http.Client{
		Transport: tr,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	if t.o.Redirects {
		client.CheckRedirect = nil
	}

	return client
}

func (t *Testsuite) DialContext(ctx context.Context, network, addr string) (net.Conn, error) {
	for from, to := range t.o.ResolveMap {
		if addr == from {
			addr = to
		}
	}
	return dialer.DialContext(ctx, network, addr)
}

func (t *Testsuite) runTestCase(tc *Testcase) error {
	req, err := http.NewRequest(tc.Method, tc.URL, nil)
	if err != nil {
		return err
	}
	cli := t.NewHttpClientForTestcase(tc)
	resp, err := cli.Do(req)
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return err
	}
	err = tc.ResponseCodeMatch.MatchToWithError(strconv.Itoa(resp.StatusCode))
	if err != nil && err != textutils.ErrEmptyStringMatch {
		return fmt.Errorf("response code %d %v", resp.StatusCode, err)
	}
	err = tc.ResponseBodyMatch.MatchToWithError(string(body))
	if err != nil && err != textutils.ErrEmptyStringMatch {
		return fmt.Errorf("response body %v", err)
	}
	for name, match := range tc.HeadersMatch {
		val := resp.Header.Get(name)
		err := match.MatchToWithError(val)
		if err != nil && err != textutils.ErrEmptyStringMatch {
			return fmt.Errorf("header %s %v", name, err)
		}
	}
	return nil
}

type TestsuiteSummary struct {
	Total  int
	Passed int
	Failed int
}

func (t *Testsuite) Run() *TestsuiteSummary {
	r := &TestsuiteSummary{
		Total: len(t.cases),
	}
	for _, tc := range t.cases {
		err := t.runTestCase(tc)
		if err != nil {
			fields := logrus.Fields{
				"url": tc.URL,
			}
			if tc.TLS > 0 {
				fields["tls"] = tc.TLSVersion
			}
			t.log.WithFields(fields).Error(err)
			r.Failed++
		} else {
			r.Passed++
		}
	}
	return r
}
