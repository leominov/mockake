package main

import (
	"net"
	"strconv"
	"strings"
)

const (
	portDefault         = 80
	pointerOptionsStart = "@options"
)

type Options struct {
	Insecure   bool
	Redirects  bool
	ResolveMap map[string]string
}

func NewOptions() *Options {
	return &Options{
		ResolveMap: make(map[string]string),
	}
}

func setDefaultPort(str string, port int) string {
	if _, _, err := net.SplitHostPort(str); err != nil {
		str += ":" + strconv.Itoa(port)
	}
	return str
}

func (o *Options) EnrichFromLine(str string) {
	pos := strings.Index(str, pointerOptionsStart)
	if pos == -1 {
		return
	}

	str = strings.TrimPrefix(str[pos:], pointerOptionsStart)
	str = strings.TrimSpace(str)
	if len(str) == 0 {
		return
	}

	data := strings.Split(strings.TrimSpace(str), " ")
	for _, pairLine := range data {
		pair := strings.Split(pairLine, "=")
		if len(pair) <= 1 {
			continue
		}
		switch pair[0] {
		case "redirects":
			ok, _ := strconv.ParseBool(pair[1])
			o.Redirects = ok
		case "insecure":
			ok, _ := strconv.ParseBool(pair[1])
			o.Insecure = ok
		case "resolve":
			dataPair := strings.Split(pair[1], "/")
			if len(dataPair) <= 1 {
				continue
			}
			key := dataPair[0]
			val := dataPair[1]
			key = setDefaultPort(key, portDefault)
			val = setDefaultPort(val, portDefault)
			o.ResolveMap[key] = val
		}
	}
}
