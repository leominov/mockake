package main

import (
	"flag"

	"github.com/sirupsen/logrus"
)

var (
	configFile = flag.String("c", ".httptest.yaml", "Path to configuration file")
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})
}

func main() {
	flag.Parse()
	logger := logrus.WithField("source", "httptest")
	ts, err := NewTestsuiteFromFile(logger, *configFile)
	if err != nil {
		logger.Fatal(err)
	}
	summary := ts.Run()
	if summary.Failed > 0 {
		logger.Fatalf("Failed %d/%d", summary.Failed, summary.Total)
	}
	logger.Infof("Passed %d/%d", summary.Passed, summary.Total)
}
