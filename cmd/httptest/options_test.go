package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOptions_EnrichFromLine(t *testing.T) {
	tests := []struct {
		in  []string
		out *Options
	}{
		{
			in: []string{
				"@options",
			},
			out: NewOptions(),
		},
		{
			in: []string{
				"@options foobar",
			},
			out: NewOptions(),
		},
		{
			in: []string{
				"@options foobar=AAA/BBB insecure=false",
			},
			out: &Options{
				ResolveMap: map[string]string{},
			},
		},
		{
			in: []string{
				"@options resolve=AAA/BBB insecure=true redirects=1",
			},
			out: &Options{
				Redirects: true,
				Insecure:  true,
				ResolveMap: map[string]string{
					"AAA:80": "BBB:80",
				},
			},
		},
		{
			in: []string{
				"@options resolve=EEE:443/FFF:443 resolve=GGG",
				"@options resolve=HHH:80/III:80",
			},
			out: &Options{
				ResolveMap: map[string]string{
					"EEE:443": "FFF:443",
					"HHH:80":  "III:80",
				},
			},
		},
	}
	for _, test := range tests {
		o := NewOptions()
		for _, line := range test.in {
			o.EnrichFromLine(line)
		}
		assert.Equal(t, test.out, o)
	}
}
