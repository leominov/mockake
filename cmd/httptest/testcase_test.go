package main

import (
	"crypto/tls"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/leominov/mockake/pkg/textutils"
)

func TestNewTestcaseFromLine(t *testing.T) {
	tests := []struct {
		in  string
		out *Testcase
	}{
		{
			in: "@test url=https://google.com/service body.exact=service",
			out: &Testcase{
				Method:            http.MethodGet,
				URL:               "https://google.com/service",
				ResponseBodyExact: "service",
				ResponseBodyMatch: textutils.StringMatch{
					Exact: "service",
				},
			},
		},
		{
			in: "@test url=https://google.com/service body.exact=service code.exact=500",
			out: &Testcase{
				Method:            http.MethodGet,
				URL:               "https://google.com/service",
				ResponseBodyExact: "service",
				ResponseBodyMatch: textutils.StringMatch{
					Exact: "service",
				},
				ResponseCodeExact: "500",
				ResponseCodeMatch: textutils.StringMatch{
					Exact: "500",
				},
			},
		},
		{
			in: "@test url=https://google.com/service body=service code=500",
			out: &Testcase{
				Method:               http.MethodGet,
				URL:                  "https://google.com/service",
				ResponseBodyExactAlt: "service",
				ResponseBodyMatch: textutils.StringMatch{
					Exact: "service",
				},
				ResponseCodeExactAlt: "500",
				ResponseCodeMatch: textutils.StringMatch{
					Exact: "500",
				},
			},
		},
		{
			in: "@test url=https://google.com/service body.exact=service code.exact=500 tls=1.2",
			out: &Testcase{
				Method:            http.MethodGet,
				URL:               "https://google.com/service",
				ResponseBodyExact: "service",
				ResponseBodyMatch: textutils.StringMatch{
					Exact: "service",
				},
				ResponseCodeExact: "500",
				ResponseCodeMatch: textutils.StringMatch{
					Exact: "500",
				},
				TLSVersion: "1.2",
				TLS:        tls.VersionTLS12,
			},
		},
		{
			in: "@test url=https://google.com/service body.prefix=service code.exact=100 tls=2.0 retries=10 timeout=15",
			out: &Testcase{
				Method:             http.MethodGet,
				URL:                "https://google.com/service",
				ResponseBodyPrefix: "service",
				ResponseBodyMatch: textutils.StringMatch{
					Prefix: "service",
				},
				ResponseCodeExact: "100",
				ResponseCodeMatch: textutils.StringMatch{
					Exact: "100",
				},
				MaxRetries:     10,
				TimeoutSeconds: 15,
			},
		},
		{
			in: "@test url=https://google.com/service method=POST body.regex=.*service.* code.exact=520 tls=1.3 retries=10",
			out: &Testcase{
				Method:            http.MethodPost,
				URL:               "https://google.com/service",
				ResponseBodyRegex: ".*service.*",
				ResponseCodeExact: "520",
				ResponseCodeMatch: textutils.StringMatch{
					Exact: "520",
				},
				ResponseBodyMatch: textutils.StringMatch{
					Regex: ".*service.*",
				},
				TLSVersion: "1.3",
				TLS:        tls.VersionTLS13,
				MaxRetries: 10,
			},
		},
		{
			in:  "@test url=https://google.com/service body.exact=service code.exact=ABC tls=1.3 retries=DEF",
			out: nil,
		},
		{
			in:  "@test body=service code=304",
			out: nil,
		},
		{
			in:  "url=https://google.com/service body.exact=service code.exact=ABC tls=1.3 retries=DEF",
			out: nil,
		},
		{
			in:  "@test",
			out: nil,
		},
	}
	for _, test := range tests {
		out, _ := NewTestcaseFromLine(test.in)
		assert.Equal(t, test.out, out)
	}
}
