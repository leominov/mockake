package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"github.com/leominov/mockake/pkg/textutils"
)

func TestNewTestsuiteFromFile(t *testing.T) {
	log := logrus.New().WithField("test", true)

	_, err := NewTestsuiteFromFile(log, "test_data/not_found.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "no such file or directory")
	}

	_, err = NewTestsuiteFromFile(log, "test_data/invalid.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "tests not found")
	}

	ts, err := NewTestsuiteFromFile(log, "test_data/valid.yaml")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, 1, len(ts.o.ResolveMap))
	if !assert.Equal(t, 49, len(ts.cases)) {
		return
	}
	assert.Equal(t, &Testcase{
		Method:            http.MethodGet,
		ResponseBodyExact: "resources-legacy-api",
		ResponseBodyMatch: textutils.StringMatch{
			Exact: "resources-legacy-api",
		},
		URL: "https://google.com/resources-legacy-api",
	}, ts.cases[0])
}

func TestTestsuite_Run(t *testing.T) {
	log := logrus.New().WithField("test", true)

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("FOOBAR", "ABCD")
		w.Write([]byte(`ABCD`))
	}))
	defer s.Close()

	ts := &Testsuite{
		log: log,
		o:   NewOptions(),
	}
	cases := []string{
		fmt.Sprintf("@test url=%s body=ABCD", s.URL),
		fmt.Sprintf("@test url=%s body.exact=ABCD", s.URL),
		fmt.Sprintf("@test url=%s body.prefix=AB", s.URL),
		fmt.Sprintf("@test url=%s body.prefix=ABC", s.URL),
		fmt.Sprintf("@test url=%s body.regex=.*BCD.*", s.URL),
		fmt.Sprintf("@test url=%s body.regex=ABCD", s.URL),
		fmt.Sprintf("@test url=%s headers.FOOBAR=ABCD", s.URL),
		fmt.Sprintf("@test url=%s headers.FOOBAR.exact=ABCD", s.URL),
		fmt.Sprintf("@test url=%s headers.FOOBAR.prefix=AB", s.URL),
		fmt.Sprintf("@test url=%s headers.FOOBAR.prefix=ABC", s.URL),
		fmt.Sprintf("@test url=%s headers.FOOBAR.regex=.*BCD.*", s.URL),
		fmt.Sprintf("@test url=%s headers.FOOBAR.regex=ABCD", s.URL),
		fmt.Sprintf("@test url=%s code=200", s.URL),
		fmt.Sprintf("@test url=%s code.exact=200", s.URL),
		fmt.Sprintf("@test url=%s code.prefix=2", s.URL),
		fmt.Sprintf("@test url=%s code.prefix=20", s.URL),
		fmt.Sprintf("@test url=%s code.regex=.*0.*", s.URL),
		fmt.Sprintf("@test url=%s code.regex=200", s.URL),
		fmt.Sprintf("@test url=%s body=ABCD headers.FOOBAR=ABCD code=200", s.URL),
	}
	for _, line := range cases {
		tc, _ := NewTestcaseFromLine(line)
		ts.cases = append(ts.cases, tc)
	}
	report := ts.Run()
	assert.Zero(t, report.Failed)
	assert.Equal(t, len(cases), report.Passed)
	assert.True(t, report.Passed == report.Total)

	ts = &Testsuite{
		log: log,
		o:   NewOptions(),
	}
	tc, _ := NewTestcaseFromLine(fmt.Sprintf("@test url=%s body.exact=1234", s.URL))
	err := ts.runTestCase(tc)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "body not equal")
	}

	ts = &Testsuite{
		log: log,
		o:   NewOptions(),
	}
	tc, _ = NewTestcaseFromLine(fmt.Sprintf("@test url=%s code.exact=1234", s.URL))
	err = ts.runTestCase(tc)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "response code 200 not equal to 1234")
	}

	ts = &Testsuite{
		log: log,
		o:   NewOptions(),
	}
	tc, _ = NewTestcaseFromLine(fmt.Sprintf("@test url=%s body.prefix=1234", s.URL))
	err = ts.runTestCase(tc)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "body does not contains prefix")
	}

	ts = &Testsuite{
		log: log,
		o:   NewOptions(),
	}
	tc, _ = NewTestcaseFromLine(fmt.Sprintf("@test url=%s body.regex=.*1234.*", s.URL))
	err = ts.runTestCase(tc)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "body does not match")
	}

	ts = &Testsuite{
		log: log,
		o:   NewOptions(),
	}
	tc, _ = NewTestcaseFromLine(fmt.Sprintf("@test url=%s body.regex=.*1234.*", s.URL))
	ts.cases = append(ts.cases, tc)
	report = ts.Run()
	assert.Equal(t, 1, report.Total)
	assert.Equal(t, 1, report.Failed)
	assert.Equal(t, 0, report.Passed)
}
