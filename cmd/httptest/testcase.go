package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"

	"github.com/mitchellh/mapstructure"

	"github.com/leominov/mockake/pkg/textutils"
)

const (
	pointerTestStart = "@test"
)

var (
	tlsSupportMap = map[string]uint16{
		"1.0": tls.VersionTLS10,
		"1.1": tls.VersionTLS11,
		"1.2": tls.VersionTLS12,
		"1.3": tls.VersionTLS13,
	}
)

type Testcase struct {
	CustomFields         map[string]interface{}           `mapstructure:",remain"`
	HeadersMatch         map[string]textutils.StringMatch `mapstructure:"-"`
	MaxRetries           int                              `mapstructure:"retries"`
	Method               string                           `mapstructure:"method"`
	ResponseBodyExact    string                           `mapstructure:"body.exact"`
	ResponseBodyExactAlt string                           `mapstructure:"body"`
	ResponseBodyMatch    textutils.StringMatch            `mapstructure:"-"`
	ResponseBodyPrefix   string                           `mapstructure:"body.prefix"`
	ResponseBodyRegex    string                           `mapstructure:"body.regex"`
	ResponseCodeExact    string                           `mapstructure:"code.exact"`
	ResponseCodeExactAlt string                           `mapstructure:"code"`
	ResponseCodeMatch    textutils.StringMatch            `mapstructure:"-"`
	ResponseCodePrefix   string                           `mapstructure:"code.prefix"`
	ResponseCodeRegex    string                           `mapstructure:"code.regex"`
	TLS                  uint16                           `mapstructure:"-"`
	TLSVersion           string                           `mapstructure:"tls"`
	TimeoutSeconds       int                              `mapstructure:"timeout"`
	URL                  string                           `mapstructure:"url"`
}

func NewTestcaseFromLine(str string) (*Testcase, error) {
	pos := strings.Index(str, pointerTestStart)
	if pos == -1 {
		return nil, nil
	}

	str = strings.TrimPrefix(str[pos:], pointerTestStart)
	str = strings.TrimSpace(str)
	if len(str) == 0 {
		return nil, nil
	}

	data := strings.Split(strings.TrimSpace(str), " ")

	input := map[string]interface{}{}
	for _, pairLine := range data {
		pair := strings.Split(pairLine, "=")
		if len(pair) <= 1 {
			continue
		}
		input[pair[0]] = pair[1]
	}

	testcase := &Testcase{
		HeadersMatch: make(map[string]textutils.StringMatch),
	}
	config := &mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           &testcase,
	}
	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return nil, err
	}
	err = decoder.Decode(input)
	if err != nil {
		return nil, err
	}

	if len(testcase.URL) == 0 {
		return nil, nil
	}

	testcase.ResponseBodyMatch = textutils.StringMatch{
		Exact:  testcase.ResponseBodyExact,
		Prefix: testcase.ResponseBodyPrefix,
		Regex:  testcase.ResponseBodyRegex,
	}
	if testcase.ResponseBodyMatch.IsEmpty() {
		testcase.ResponseBodyMatch.Exact = testcase.ResponseBodyExactAlt
	}

	testcase.ResponseCodeMatch = textutils.StringMatch{
		Exact:  testcase.ResponseCodeExact,
		Prefix: testcase.ResponseCodePrefix,
		Regex:  testcase.ResponseCodeRegex,
	}
	if testcase.ResponseCodeMatch.IsEmpty() {
		testcase.ResponseCodeMatch.Exact = testcase.ResponseCodeExactAlt
	}

	for field, body := range testcase.CustomFields {
		sm := textutils.StringMatch{}
		data := strings.Split(field, ".")
		if len(data) <= 2 {
			continue
		}
		if !strings.HasPrefix(data[0], "headers") {
			continue
		}
		if len(data) == 2 {
			sm.Exact = body.(string)
		} else {
			switch data[2] {
			case "exact":
				sm.Exact = body.(string)
			case "prefix":
				sm.Prefix = body.(string)
			case "regex":
				sm.Regex = body.(string)
			default:
				return nil, fmt.Errorf("unknown %s match type", data[2])
			}
		}
		testcase.HeadersMatch[data[1]] = sm
	}

	if len(testcase.HeadersMatch) == 0 {
		testcase.HeadersMatch = nil
	}

	ver, ok := tlsSupportMap[testcase.TLSVersion]
	if !ok {
		testcase.TLSVersion = ""
	} else {
		testcase.TLS = ver
	}

	if len(testcase.Method) == 0 {
		testcase.Method = http.MethodGet
	}

	return testcase, nil
}
