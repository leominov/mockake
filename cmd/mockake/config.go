package main

import (
	"github.com/leominov/mockake/pkg/textutils"
)

type Config struct {
	Interactions []*Interaction `yaml:"interactions"`
}

type Interaction struct {
	Request  *InteractionRequest  `yaml:"request"`
	Response *InteractionResponse `yaml:"response"`
}

type InteractionRequest struct {
	Path    textutils.StringMatch            `yaml:"path"`
	Method  textutils.StringMatch            `yaml:"method"`
	Headers map[string]textutils.StringMatch `yaml:"headers"`
}

type InteractionResponse struct {
	Body    string              `yaml:"body"`
	Headers map[string][]string `yaml:"headers"`
	Status  int                 `yaml:"statusCode"`
}
