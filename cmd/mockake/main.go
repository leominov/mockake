package main

import (
	"flag"
	"fmt"
)

var (
	listenAddress = flag.String("bind", ":8088", "Default address to serve incoming requests")
	configFile    = flag.String("config", "", "Configuration file")
)

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) < 1 {
		fmt.Println("command must be specified")
		return
	}
	command := args[0]
	doneCh := make(chan bool)
	mock := NewMock(*listenAddress, doneCh)
	switch command {
	case "start":
		if ok, _ := mock.IsRunning(); ok {
			fmt.Println("already running")
			return
		}
		fmt.Println("Starting mockake...")
		if len(*configFile) > 0 {
			err := mock.loadFromFile(*configFile)
			if err != nil {
				fmt.Printf("failed to load config: %v\n", err)
				return
			}
			fmt.Printf("Configuration file: %s\n", *configFile)
		} else {
			fmt.Println("Configuration is empty, use `set` command")
		}
		go func() {
			fmt.Printf("Listen address: %s\n", *listenAddress)
			err := mock.Serve()
			if err != nil {
				fmt.Println(err)
				close(doneCh)
			}
		}()
		<-doneCh
	case "stop":
		if ok, err := mock.Stop(); !ok {
			fmt.Printf("failed to stop: %v\n", err)
			return
		}
		fmt.Println("ok")
	case "status":
		if ok, _ := mock.IsRunning(); !ok {
			fmt.Println("stopped")
			return
		}
		fmt.Println("running")
	case "flush":
		if ok, err := mock.Flush(); !ok {
			fmt.Printf("failed to flush: %v\n", err)
			return
		}
		fmt.Println("ok")
	case "set":
		if len(*configFile) == 0 {
			fmt.Println("config file must be specified")
			return
		}
		if ok, err := mock.Set(*configFile); !ok {
			fmt.Printf("failed to set: %v\n", err)
			return
		}
		fmt.Println("ok")
	default:
		fmt.Println("incorrect command")
	}
}
