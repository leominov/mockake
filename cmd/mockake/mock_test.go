package main

import (
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewMock_loadFromFile(t *testing.T) {
	doneCh := make(chan bool)
	m := NewMock(":8080", doneCh)
	err := m.loadFromFile("test_data/not-found.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "no such file")
	}
	err = m.loadFromFile("test_data/invalid.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "yaml")
	}
	err = m.loadFromFile("test_data/valid.yaml")
	assert.NoError(t, err)
}

func TestMock_Serve(t *testing.T) {
	doneCh := make(chan bool)
	m := NewMock(":8082", doneCh)
	err := m.loadFromFile("test_data/serve.yaml")
	if !assert.NoError(t, err) {
		return
	}
	go func() {
		err := m.Serve()
		if err != nil {
			t.Fatal(err)
		}
	}()
	defer close(m.doneCh)

	time.Sleep(100 * time.Millisecond)

	resp, err := http.Get("http://127.0.0.1:8082/exact")
	if !assert.NoError(t, err) {
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusOK)
	assert.Equal(t, string(body), `{"status_code":"ok"}`)
	assert.Equal(t, resp.Header.Get("Foo"), "Bar")

	resp, err = http.Get("http://127.0.0.1:8082/exact/foobar")
	if !assert.NoError(t, err) {
		return
	}
	_, err = ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNotFound)

	resp, err = http.Post("http://127.0.0.1:8082/prefix", "", nil)
	if !assert.NoError(t, err) {
		return
	}
	_, err = ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNoContent)

	resp, err = http.Post("http://127.0.0.1:8082/prefix/foobar", "", nil)
	if !assert.NoError(t, err) {
		return
	}
	_, err = ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNoContent)

	resp, err = http.Post("http://127.0.0.1:8082/regex", "", nil)
	if !assert.NoError(t, err) {
		return
	}
	_, err = ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNoContent)

	resp, err = http.Post("http://127.0.0.1:8082/aaa-regex-bbb", "", nil)
	if !assert.NoError(t, err) {
		return
	}
	_, err = ioutil.ReadAll(resp.Body)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNoContent)
}
