package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/gorilla/handlers"
	yaml "gopkg.in/yaml.v2"
)

const (
	SetRoute    = "/-/set"
	StatusRoute = "/-/status"
	StopRoute   = "/-/stop"
	FlushRoute  = "/-/flush"
)

var (
	ErrIncorrectResponseCode = errors.New("incorrect response code")
	ErrIncorrectResponseText = errors.New("incorrect response text")
	ErrAlreadyStopped        = errors.New("already stopped")
)

type Mock struct {
	listenAddr  string
	requestAddr string
	doneCh      chan bool
	sync.Mutex
	its []*Interaction
}

func NewMock(listenAddress string, doneCh chan bool) *Mock {
	m := &Mock{
		listenAddr: listenAddress,
		doneCh:     doneCh,
	}
	m.requestAddr = m.getRequestAddr()
	return m
}

func (m *Mock) loadFromFile(filename string) error {
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	c := &Config{}
	err = yaml.Unmarshal(body, &c)
	if err != nil {
		return err
	}
	m.its = c.Interactions
	return nil
}

func (m *Mock) getInteractionByRequest(r *http.Request) *Interaction {
	m.Lock()
	defer m.Unlock()
	for _, it := range m.its {
		if !it.Request.Path.MatchTo(r.RequestURI) ||
			!it.Request.Method.MatchTo(r.Method) {
			continue
		}
		for header, cond := range it.Request.Headers {
			val := r.Header.Get(header)
			if !cond.MatchTo(val) {
				return nil
			}
		}
		return it
	}
	return nil
}

func (m *Mock) MockHandler(w http.ResponseWriter, r *http.Request) {
	it := m.getInteractionByRequest(r)
	if it == nil {
		http.Error(w, "Not found.", http.StatusNotFound)
		return
	}
	header := w.Header()
	status := it.Response.Status
	if status == 0 {
		status = http.StatusOK
	}
	for name, vals := range it.Response.Headers {
		for _, val := range vals {
			header.Add(name, val)
		}
	}
	w.WriteHeader(status)
	w.Write([]byte(it.Response.Body))
}

func (m *Mock) StopHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodHead {
		http.Error(w, "Bad request.", http.StatusBadRequest)
		return
	}
	close(m.doneCh)
}

func (m *Mock) SetHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Bad request.", http.StatusBadRequest)
	}
	c := &Config{}
	decoder := yaml.NewDecoder(r.Body)
	err := decoder.Decode(&c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if c == nil {
		http.Error(w, "Empty body.", http.StatusBadRequest)
		return
	}
	m.Lock()
	m.its = c.Interactions
	m.Unlock()
	w.Write([]byte(`ok`))
}

func (m *Mock) FlushHandler(w http.ResponseWriter, r *http.Request) {
	m.Lock()
	m.its = []*Interaction{}
	m.Unlock()
	w.Write([]byte(`ok`))
}

func (m *Mock) StatusHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`ok`))
}

func (m *Mock) Serve() error {
	r := http.NewServeMux()
	r.HandleFunc(SetRoute, m.SetHandler)
	r.HandleFunc(StatusRoute, m.StatusHandler)
	r.HandleFunc(StopRoute, m.StopHandler)
	r.HandleFunc(FlushRoute, m.FlushHandler)
	r.HandleFunc("/", m.MockHandler)
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	return http.ListenAndServe(m.listenAddr, loggedRouter)
}

func (m *Mock) getRequestAddr() string {
	if strings.HasPrefix(m.listenAddr, ":") || strings.HasPrefix(m.listenAddr, "0.0.0.0") {
		hostPort := strings.Split(m.listenAddr, ":")
		if len(hostPort) == 2 {
			return fmt.Sprintf("127.0.0.1:%s", hostPort[1])
		}
	}
	return m.listenAddr
}

func (m *Mock) IsRunning() (bool, error) {
	addr := "http://" + m.requestAddr + StatusRoute
	resp, err := http.Get(addr)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	status := string(body)
	if status == "ok" {
		return true, nil
	}
	return false, ErrIncorrectResponseText
}

func (m *Mock) Stop() (bool, error) {
	if ok, _ := m.IsRunning(); !ok {
		return false, ErrAlreadyStopped
	}
	addr := "http://" + m.requestAddr + StopRoute
	resp, err := http.Head(addr)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return false, ErrIncorrectResponseCode
	}
	return true, nil
}

func (m *Mock) Set(path string) (bool, error) {
	if ok, _ := m.IsRunning(); !ok {
		return false, ErrAlreadyStopped
	}
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return false, err
	}
	addr := "http://" + m.requestAddr + SetRoute
	resp, err := http.Post(addr, "", bufio.NewReader(bytes.NewBuffer(b)))
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return false, ErrIncorrectResponseCode
	}
	return true, nil
}

func (m *Mock) Flush() (bool, error) {
	if ok, _ := m.IsRunning(); !ok {
		return false, ErrAlreadyStopped
	}
	addr := "http://" + m.requestAddr + FlushRoute
	resp, err := http.Post(addr, "", nil)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return false, ErrIncorrectResponseCode
	}
	return true, nil
}
