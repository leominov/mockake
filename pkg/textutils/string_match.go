package textutils

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

var (
	ErrEmptyStringMatch = errors.New("empty match rule")
)

type StringMatch struct {
	Exact  string `yaml:"exact"`
	Prefix string `yaml:"prefix"`
	Regex  string `yaml:"regex"`
}

func (s *StringMatch) IsEmpty() bool {
	return len(s.Exact) == 0 && len(s.Prefix) == 0 && len(s.Regex) == 0
}

func (s *StringMatch) MatchToWithError(text string) error {
	if s.IsEmpty() {
		return ErrEmptyStringMatch
	}
	if len(s.Exact) > 0 {
		if strings.Compare(s.Exact, text) == 0 {
			return nil
		}
		return fmt.Errorf("not equal to %s", s.Exact)
	}
	if len(s.Prefix) > 0 {
		if strings.HasPrefix(text, s.Prefix) {
			return nil
		}
		return fmt.Errorf("does not contains prefix %s", s.Prefix)
	}
	re, err := regexp.Compile(s.Regex)
	if err != nil {
		return fmt.Errorf("invalid regex: %s", s.Regex)
	}
	if re.MatchString(text) {
		return nil
	}
	return fmt.Errorf("does not match to %s", s.Regex)
}

func (s *StringMatch) MatchTo(text string) bool {
	return s.MatchToWithError(text) == nil
}
