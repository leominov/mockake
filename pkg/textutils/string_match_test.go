package textutils

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	tests = []struct {
		in  string
		sm  StringMatch
		err error
	}{
		{
			err: ErrEmptyStringMatch,
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Exact: "ABCD",
			},
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Exact: "ABCDE",
			},
			err: errors.New("not equal to ABCDE"),
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Exact: "ABC",
			},
			err: errors.New("not equal to ABC"),
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Prefix: "ABCD",
			},
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Prefix: "ABC",
			},
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Prefix: "ABCDE",
			},
			err: errors.New("does not contains prefix ABCDE"),
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Regex: "ABCD",
			},
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Regex: "ABC.*",
			},
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Regex: ".*BC.*",
			},
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Regex: ".*BC.*DE",
			},
			err: errors.New("does not match to .*BC.*DE"),
		},
		{
			in: "ABCD",
			sm: StringMatch{
				Regex: ".*'({$/BC.*DE",
			},
			err: errors.New("invalid regex: .*'({$/BC.*DE"),
		},
	}
)

func TestStringMatch_MatchToWithError(t *testing.T) {
	for _, test := range tests {
		assert.Equal(t, test.err, test.sm.MatchToWithError(test.in))
	}
}

func TestStringMatch_MatchTo(t *testing.T) {
	for _, test := range tests {
		assert.Equal(t, test.err == nil, test.sm.MatchTo(test.in))
	}
}
