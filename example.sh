#!/bin/bash

set -o pipefail

check_status() {
    if [[ -z "$API_HOST" ]] ; then
        return 1
    fi
    if [[ -z "$API_TOKEN" ]] ; then
        return 1
    fi
    if ! hash curl ; then
        return 1
    fi
    if ! hash jq ; then
        return 1
    fi
    status="$(curl -sf -H "PRIVATE-TOKEN: $API_TOKEN" "http://$API_HOST/get_status" | jq -r '.status_code')"
    if [[ "$?" != 0 ]] ; then
        return 2
    fi
    if [[ "$status" != "ok" ]] ; then
        return 3
    fi
}
