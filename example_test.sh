#!/bin/bash

curl -sfL https://raw.githubusercontent.com/kward/shunit2/master/shunit2 -o shunit2
go build
./mockake start > /dev/null &

test_check_status() {
    API_HOST=127.0.0.1:8088 check_status
    assertEquals "validation failed" "$?" 1

    ./mockake --config ./example_test_ok.yaml set
    API_HOST=127.0.0.1:8088 API_TOKEN=ABCD check_status
    assertEquals "success" "$?" 0

    ./mockake flush
    API_HOST=127.0.0.1:8088 API_TOKEN=ABCD check_status
    assertEquals "failed by response code" "$?" 2

    ./mockake --config ./example_test_error_token.yaml set
    API_HOST=127.0.0.1:8088 API_TOKEN=DCBA check_status
    assertEquals "failed by invalid token" "$?" 2

    ./mockake --config ./example_test_error_status.yaml set
    API_HOST=127.0.0.1:8088 API_TOKEN=ABCD check_status
    assertEquals "failed by status code" "$?" 3

    ./mockake stop
}

. example.sh
. shunit2
